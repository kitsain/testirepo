import React, { useState, useEffect } from "react";
import DatePicker from "react-datepicker";
import { BiTrash } from "react-icons/bi";
import "react-datepicker/dist/react-datepicker.css";
import { registerLocale } from  "react-datepicker";
import Image from './whatsapp.png';
import Items from "./Items.js"
import fi from 'date-fns/locale/fi';
registerLocale('fi', fi)


const DateChooser = (props) => {
  // Date state, defaults as current date, changes when user chooses another date
  const [startDate, setStartDate] = useState(new Date());
  const [productName, setProductName] = useState(props.productName);
  const [productEan, setProductEan] = useState(props.ean);
  const [allItems, setItems] = useState([]);
  const [ostosKori, AddToCart] = useState([]);
  const [used, setUsed] = useState([]);
  const [wasted, Addwasted] = useState([]);

  // Handels the datepicker components date change, parses the date and sends it to result components through props
  const handleDateChange = (date) => {
    date.setHours(12, 30, 0, 0)
    setStartDate(date)
    let parsedDate = parseDate(date)
    props.sendDate(parsedDate)
  }

  useEffect(() => {
    getData();
    handleDateChange(startDate)
  }, []);

  useEffect(() => {
    if (props.ean || props.productName) {
      setProductName(props.productName);
      setProductEan(props.ean);
    }
  }, [props.productName,props.ean])



  //datan hakemminen localstoragesta
  const getData = async () => {
    let array = ["items", "ostoskori", "used", "waste"]
    for (let i = 0; i < array.length; i++) {
      try {
        const value = await localStorage.getItem(array[i])
        // console.log(JSON.stringify(value))

        if (value !== null) {
          switch (array[i]) {
            case "items":
              setItems(JSON.parse(value));
              break;
            case "ostoskori":
              AddToCart(JSON.parse(value));
              break;
            case "used":
              setUsed(JSON.parse(value));
              break;
            case "waste":
              Addwasted(JSON.parse(value));
              break;
            default:
            // code block
          }
        }

      } catch (e) {
        console.log(e);
      }
    }

  }
  //datan tallennus välimuistiin
  const storeData = async () => {
    let array = ["items", "ostoskori", "used", "waste"]
    for (let i = 0; i < array.length; i++)
      try {
        switch (array[i]) {
          case "items":
            await localStorage.setItem(array[i], JSON.stringify(allItems));
            break;
          case "ostoskori":
            await localStorage.setItem(array[i], JSON.stringify(ostosKori));
            break;
          case "used":
            await localStorage.setItem(array[i], JSON.stringify(used));
            break;
          case "waste":
            await localStorage.setItem(array[i], JSON.stringify(wasted));
            break;
          default:
          // code block
        }

        // console.log("saved")
        // console.log(productEan)
      } catch (e) {
        alert(e)
      }

  }


  useEffect(() => {
    storeData();
  }, [allItems, ostosKori, used, wasted]);



  // Parses the date to ISO format and slice&split it to year, month, day
  const parseDate = (date) => {
    let formatted = date.toISOString().slice(0, 10).split('-')
    return [formatted[0], formatted[1], formatted[2]]
  }

const chooseList =(listaId) => {
  //items.js:ltä saatu listaId ja lähetetään se eteenpäin addItemiin
additem(productName,startDate,productEan,listaId)
 
}
const additem = (item, date, ean,lista) => {
  let kpl = 1;
  console.log(lista)
  if (item !== "" || ean !== "") {
    // let newDate = new Intl.DateTimeFormat(['ban', 'id']).format(date)
    // let lol = JSON.stringify(date)
    const newItem = { "tuote": item, "bb": JSON.stringify(date), EAN: ean, checked:false}

    //  console.log(date);
    //const items = [...allItems, newItem]
    try{
    switch (lista) {
      case "T":
        setItems([...allItems, newItem]);
        break;
      case "O":
        AddToCart([...ostosKori, newItem])
        break;
      case "H":
        Addwasted([...wasted, newItem])
        break;
    }
  }
  catch (e)
 { console.log(e)}
  
    setProductName("")
    setProductEan("")
    //setItems(items)


    //HISTORIALISTAAN LISÄYS
    const tuoteIndex = used.findIndex((obj => obj.tuote === item))
    const eanIndex = used.findIndex((obj => obj.EAN === ean))
    console.log(tuoteIndex)
    console.log(eanIndex)
    //katsotaan onko lisättävä tuote jo listassa
    if (tuoteIndex === -1 && eanIndex === -1) {
      const roskakori = [...used, { tuote: item, bb: JSON.stringify(date), EAN: ean, kpl: kpl,checked:false }]
      setUsed(roskakori)
      console.log(eanIndex)
    }
    //jos on jo kyseinen ean koodi ilman tuotenimeä
    else if (eanIndex !== -1 && used[eanIndex].EAN !== "") {
      used[eanIndex].kpl = used[eanIndex].kpl + 1
      if (item !== "")
        used[eanIndex].tuote = item
      console.log(tuoteIndex)
      console.log(eanIndex)
    }
    //jos tuotenimi ei ole uusi
    else if (tuoteIndex !== -1) {
      used[tuoteIndex].kpl = used[tuoteIndex].kpl + 1
      if (ean !== "")
        used[tuoteIndex].EAN = ean
      console.log(tuoteIndex)
      console.log(eanIndex)
    }

    else {
      const roskakori = [...used, { tuote: item, bb: JSON.stringify(date), EAN: ean, kpl: kpl }]
      setUsed(roskakori)
    }

  }
  console.log(allItems)

}
  const removeitem = (index) => {

    var items = used;
    items.splice(index, 1)
    // console.log(items, index)
    setUsed(items)
    localStorage.setItem("used", JSON.stringify(used))
    getData()
  }
  
  
  const changeData = (newData, id) => {
    // console.log(newData)
    // console.log(id)

    //jos vaihetaan listan sisäisesti paikkoja
    if (id === "droppable") {
      setItems(newData)
    }
    else if (id === "droppable2") {
      AddToCart(newData)
    }
    else if (id === "droppable3") {
      setUsed(newData)
    }
    else if (id === "droppable4") {
      Addwasted(newData)
    }
  }
  const changeList = (source, destination, tuotelista, ostoslista, Used, waste) => {
    //  if(source === "droppable" && destination !== "droppable3")
    // setItems(tuotelista !== undefined ? allItems : allItems)
    // else
    //console.log(source, destination, tuotelista, ostoslista, Used, waste)
    setItems(tuotelista !== undefined ? tuotelista : allItems)
    AddToCart(ostoslista !== undefined ? ostoslista : ostosKori)
    //roskakori source=== 1 tapahtuu esim edittaus ja kaikkien taulujen tyhjennys
    setUsed(Used !== undefined && source===1 ? Used : used)
    Addwasted(waste !== undefined ? waste : wasted)
    storeData()
  }

  const checkField = (e) =>{
        if(e.key === 'Enter'){
          let ean = Number(e.target.value)
          if(!isNaN(ean) && ean != 0){
            props.manualSearch(ean)
          }
        }
      }  

  const resetFields = () =>{
    setProductEan('');
    setProductName('');
    setStartDate(new Date());
  }


  //console.log(ostosKori)
  let href = "whatsapp://send?text=" + productName
  let link = "https://wa.me/?text=" + encodeURIComponent(productName + "menee vanhaksi!")
        let event = {
            title: productName !== ""? productName: productEan,
            description: 'Tuotteen ' + productName + " viimeinen käyttöpäivä." + `<a href=${link}>Lähetä WhatsApp viesti!</a><br/>`,
            startTime: startDate,
            endTime: startDate,
            location: 'Keittiö'
        }
  return (
    <>
      <div style={{ display: "flex", margin: 10, flexDirection: "column", alignItems: "center" }}>
        
        Luettu viivakoodi: {productEan}

        <br></br> Tuote:
        <div style={{display:'inline-flex'}}>
        <input autoFocus placeholder="tuotenimi" style={{ fontSize:15, height: "3vh", width: 250 }} onChange={(e) => setProductName(e.target.value)} value={productName} onKeyDown={checkField}></input>
        <button onClick={resetFields} style={{width:'4vh',margin:0, marginLeft:3}}><BiTrash/></button>
        </div>
        <br></br>
      
   Parasta ennen / viimeinen käyttöpäivä
<DatePicker dateFormat="dd/MM/yyyy"  selected={startDate} onChange={date => handleDateChange(date)} />

 
        <div>
          <div style={{ paddingTop: '10px' }}>
            <img style={{ verticalAlign: "middle", paddingLeft: 3 }} src={Image} width="25" alt='Kitsain'></img>
            <a style={{ paddingLeft: "10px", color: "white", fontWeight: "bold" }} href={href} rel="noreferrer" target="_blank">Lähetä tuote Whatsappissa</a>
          </div>
          
          {/* <AddToCalendar buttonLabel="Lisää tuote kalenteriin" event={event}  buttonTemplate={icon} /> */}
          <Items removeitem={removeitem} wasted={wasted} used={used} changeList={changeList} event={event} changeData={changeData} ostosKori={ostosKori} tuotelista={allItems} additem={chooseList}></Items>
        </div>

      </div>
    </>
  );
}
export default DateChooser

