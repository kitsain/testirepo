import React, { useEffect } from 'react';
import DateChooser from './DateChooser';
//import Items from "./Items.js;
import Tabletop from "tabletop";
import { set } from 'date-fns';
const cheerio = require("cheerio")

const Result = (props) => {
  // result holds the scanned object got from app component through props
  const result = props.result
  // state used if the scraping did not bring any products
  const [failScrape, setFailScrape] = React.useState(false)
  // State used when EAN is being scraped
  const [scraping, setScraping] = React.useState(false)
  // Holds the product name scraped from the url using EAN
  const [productName, setProductName] = React.useState('')
  const [productEan, setProductEan] = React.useState('')
  // Callback function from Datechooser, sends the parsed date to App component
  const[error, setError] = React.useState('')
  // Date mover between components
  const getDate = (date) => {
    props.sendDate(date)
  }

  // Method for different productsearches, K-ryhmä and S-ryhmä uses scraping
  const getHTML = async (EAN, shop) => {
    console.log('Getting html')
    let baseurl

    // Switch for for different productsearches based on shop given
    switch (shop) {
      // Local storage
      case 'L':
        getLocal(EAN)
        return
      // Openfood
      case 'O':
        getOpenFood(EAN)
        return
      // K-ryhmä
      case 'K':
        baseurl = 'https://kitsain-cors.herokuapp.com/https://www.k-ruoka.fi/kauppa/tuotehaku?haku=';
        break;
      // S-ryhmä
      case 'S':
        baseurl = 'https://kitsain-cors.herokuapp.com/https://www.foodie.fi/entry/';
        break;
      // Lidl/Google sheets
      case 'LI':
        getLidlSheet(EAN)
        return
      default:
        return
    }
    let url = baseurl + EAN
    try {
        // Fetch (GET) using the url
        await fetch(url).then(async response => {
        // Grab the html
        let html = await response.text()
        console.log("Fetched")
        // Get the product name via scrapeProduct
        let name = await scrapeProduct(html, shop, EAN)
        // scrapeProduct returns undefined if no product was found. If so, just return because method is being called recursively
        if (name === undefined) { return }
        // Take the productname to scrapedone method
        scrapeDone(name)
      });
    }
    catch (error) {
      console.log('Error in getHTML method', error)
      return
      //setError('Tuotteen haku epäonnistui!')
      //setFailScrape(true)
      //setGotProduct(true)
      //setScraping(false)
     }
  }

  // Scrapes the given html body to get the product name
  const scrapeProduct = async (html, shop, EAN) => {
    let name
    // Read the html using cheerio
    let $ = cheerio.load(html);
    console.log('Scraping')
    // Used for checking if the product was found
    let productNotFound = await checkProductNotFound($, shop)
    if (shop === 'K') {
      if (productNotFound) {
        // If product not found from k-ryhmä, try s-ryhmä
        console.log('K NOT found')
        getHTML(EAN, 'S')
        return undefined
      }
      else {
        console.log('K found')
        let main = $('.product-result-name')
        let child1 = main.children().first();
        let child2 = child1.children().first();
        name = child2.children().text(); 
        //name = $('.product-result-name>div>div>span').text();
      }
    }
    else {
      if (productNotFound) {
        // If product not found in s-ryhmä, try lidl
        console.log('S NOT found')
        getHTML(EAN, 'LI')
        
        return undefined
      }
      else {
        console.log('S found')
        name = $('#product-name').text();
        setFailScrape(false)
      }
    }
    return name
  }

  // Method to handle states when product is found
  const scrapeDone = (name) =>{
    setProductName(name)
    setScraping(false)
    
  }

 // Method for checking if the product was not found
  const checkProductNotFound = ($, shop) => {
    // Both sites give message ending with the word löytynyt, so we compare this
    let checkK = 'löytynyt'
    // S-ryhmä also gives message ending in (404), also compare this
    let checkS = '(404)'
    let searchstatus
    if (shop === 'K') {
      searchstatus = $('#app > section > section > div.shopping-list-container > div.shopping-list-items-container > div.shopping-list-side-panel > div > div > div > div > div.product-search-query > h1 > span').text()
      if (searchstatus.split(' ').pop() === checkK) return true;
      else return false;
    }
    else {
      searchstatus = $('#primary-content > div > h1').text();
      searchstatus = searchstatus.trim()
      if (searchstatus.split(' ').pop() === checkS || searchstatus.split(' ').pop() === checkK) return true;
      else return false
    }
  }

  // Method for getting data from Google sheets
  const getLidlSheet = (code) =>{
    Tabletop.init({key: '1U5W9fZXmd18bZI6gKcjYzT3JpEAnNKzQbUPNcpQRKe0', simpleSheet: true})
    .then((data) => getLidlProduct(data, code))
    .catch((err) => console.warn(err))
  }

  // Method for checking if the EAN is found in Google sheets
  const getLidlProduct = (data, code) =>{
    var row;
    var found = false;
    for (row of data){
      if(row.Code === code ){
        console.log('Sheets found')
        found = true;
        scrapeDone(row.Product)
        break;
      }
    }
    if(!found){
      console.log('Sheets NOT found')
      setFailScrape(true);
      setError('Tuotetta ei löytynyt!');
      scrapeDone('');
    }
  }

  // Method for getting product from OpenFood API
  const getOpenFood = (code) => {
    let baseurl = 'https://world.openfoodfacts.org/api/v0/product/'
    let url = baseurl + code + '.json';
    fetch(url)
    .then(response =>response.json())
    .then((jsonData) => {
    if (jsonData.status === '1'){
      console.log('Openfood found!')
      scrapeDone(jsonData.product.product_name)
    }
    else{
      console.log('Openfood NOT found!')
      getHTML(code, 'K')
    }
    })
  .catch((error) => {
    console.error(error)
  })
  }

  // Method for getting product from local storage
  const getLocal = async (code) => {
    try {
      let product;
      let found = false;
      let data = JSON.parse(localStorage.getItem('used'));
      for(product of data){
        if(product.EAN === code){
          found = true;
          console.log('Local storage found!')
          scrapeDone(product.tuote)
          break;
        }
      }
      if(!found){
        console.log('Local storage NOT found!')
        getHTML(code,'O')
      }
    } catch (e) {
      console.log(e)
    }
  }

  // Handles state changes when result is changed and start scraping if result contains scanned object
  const handleStates = () => {
    setFailScrape(false)
    if (result !== '') {
      setScraping(true)
      getHTML(result, 'L')
    }
  }

   // Triggers when result is changed
   useEffect(() => {
      handleStates()
    // eslint-disable-next-line
  }, [result]
  )
  
  const sendResult = (result) => {
    props.sendResult(result)
  }

   
  
  let ean = null
  let element = 
  <div style={{ display: (scraping ? 'none' : 'block')}} id='product'>

     <DateChooser ean={result !== undefined ? result : ""} productName={productName} sendDate={getDate} manualSearch={sendResult}/>
    
   </div>
  if (scraping) {
    ean = <div id='scraping'>Scraping...</div>
  }
  else if (failScrape){
    ean = <> <div id='EAN'></div> <div id='error' style={{color:'black'}}><b>{error}</b></div></>
  }
  return (
    <>
      {ean}
      {element}


  </>
)
}
export default Result