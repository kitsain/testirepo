import React, { Component } from 'react';
import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';


// fake data generator
// const getItems = (count, offset = 0) =>
//     {console.log(offset)
//         return(Array.from({ length: count }, (v, k) => k).map((v,k) => ({
//         id: `item-${k + offset}`,
//         content:  k
//     })))}
    


// a little function to help us with reordering the result
const reorder = (list, startIndex, endIndex) => {
    const result = Array.from(list);
    const [removed] = result.splice(startIndex, 1);
    result.splice(endIndex, 0, removed);

    return result;
};

/**
 * Moves an item from one list to another list.
 */


const move = (source, destination, droppableSource, droppableDestination) => {
    const sourceClone = Array.from(source);
    const destClone = Array.from(destination);
    const [removed] = sourceClone.splice(droppableSource.index, 1);

    destClone.splice(droppableDestination.index, 0, removed);

    const result = {};
    result[droppableSource.droppableId] = sourceClone;
    result[droppableDestination.droppableId] = destClone;
console.log(result)
    return result;
};

const getColor = (x) => {
if(x===100)
return("grey")
}
const grid = 8;



const getItemStyle = (isDragging, draggableStyle) => ({
    // some basic styles to make the items look a bit nicer
    userSelect: 'none',
    padding: grid * 2,
    margin: `0 0 ${grid}px 0`,

    // change background colour if dragging
    
    background: isDragging ? 'lightgreen' : getColor(100),

    // styles we need to apply on draggables
    ...draggableStyle
});

const getListStyle = isDraggingOver => ({
    background: isDraggingOver ? 'lightblue' : 'lightgrey',
    padding: grid,
    width: 250
});

class App extends Component {
    // state = {
    //     items: this.props.items,
    //     ostoskori: this.props.tuotelista
    // };
   
        // Don't call this.setState() here!
        state = {
          items: this.props.tuotelista,
          ostoskori: this.props.ostosKori,
          used: this.props.used,
          waste: this.props.wasted,
          is_open: true
        }
    
    /**
     * A semi-generic way to handle multiple lists. Matches
     * the IDs of the droppable container to the names of the
     * source arrays stored in the state.
     */
    id2List = {
        droppable: 'items',
        droppable2: 'ostoskori',
        droppable3: 'used',
        droppable4: 'waste',
        

       
    };
componentDidMount() {
    // console.log(this.props)
    // console.log(this.state.waste)
    // console.log(this.state.used)
    // console.log(this.props.items)
    this.setState({items:this.props.tuotelista})
    this.setState({ostoskori:this.props.ostosKori})
}
componentDidUpdate(){
    if(this.state.items !== this.props.tuotelista || this.state.ostoskori !== this.props.ostosKori|| this.state.used !== this.props.used || this.state.waste !== this.props.wasted)
    this.setState({items: this.props.tuotelista, ostoskori:this.props.ostosKori, used:this.props.used,waste:this.props.wasted})
}
    getList = id => this.state[this.id2List[id]];

    onDragEnd = result => {
        const { source, destination } = result;
console.log("dragend")
        // dropped outside the list
        if (!destination) {
            return;
        }
console.log(source.droppableId + "destiid"  + destination.droppableId)
        if (source.droppableId === destination.droppableId) {
            const items = reorder(
                this.getList(source.droppableId),
                source.index,
                destination.index
            );

            let state = { items };

            if (source.droppableId === 'droppable2') {
                state = { ostoskori: items };
            }
            else if  (source.droppableId === 'droppable3') {
                state = { used: items };
            }
            else if  (source.droppableId === 'droppable4') {
                state = { waste: items };
            }
            this.setState(state);
            console.log("changepos")
            this.props.changeData(items,source.droppableId)
        } else {
            const result = move(
                this.getList(source.droppableId),
                this.getList(destination.droppableId),
                source,
                destination
            );

            this.setState({
                items: result.droppable !== undefined ? result.droppable: this.state.items,
                ostoskori: result.droppable2 !== undefined ? result.droppable2: this.state.ostoskori,
                used: result.droppable3 !== undefined ? result.droppable3: this.state.used,
                waste: result.droppable4 !== undefined ? result.droppable4: this.state.waste,
            });
            this.props.changeList(result.droppable,result.droppable2,result.droppable3,result.droppable4)
            // this.props.changeData(result.droppable,result.droppable2)
        //    console.log("tuotelista" + result.droppable)
        //    console.log("ostoslista"+result.droppable2)
        //    console.log("used" + result.droppable3)
        //    console.log("waste"+result.droppable4)
          
        }
    };

    // Normally you would want to split things out into separate components.
    // But in this example everything is just done in one place for simplicity
    render() {
        // console.log("ostoskori" + this.state.ostoskori)
        // console.log("ostoskori" + this.props.tuotelista)
        // console.log(this.props.items)
        // console.log("tuotetelista" + this.state.items1)
        return (
            <div style={{display:"flex"}}>
            <DragDropContext onDragEnd={this.onDragEnd}>
                <div>
                Tuotelista
            
                <Droppable droppableId="droppable">
                    {(provided, snapshot) => (
                        <div
                            ref={provided.innerRef}
                            style={getListStyle(snapshot.isDraggingOver)}>
                            {this.state.items.map((item, index) => (
                                <Draggable
                                    key={index}
                                    draggableId={item.tuote + index +"T"}
                                    index={index}>
                                    {(provided, snapshot) => (
                                        <div
                                            ref={provided.innerRef}
                                            {...provided.draggableProps}
                                            {...provided.dragHandleProps}
                                            style={getItemStyle(
                                                snapshot.isDragging,
                                                provided.draggableProps.style
                                            )}>
                                                
                                            {item.tuote} {Intl.DateTimeFormat(['ban', 'id']).format(Date.parse(JSON.parse(item.bb)))}
                                        </div>
                                    )}
                                </Draggable>
                            ))}
                            {provided.placeholder}
                        </div>
                    )}
                </Droppable>
                </div>
                <div>
                Ostoslista
                <Droppable droppableId="droppable2">
                  
                    {(provided, snapshot) => (
                        <div
                            ref={provided.innerRef}
                            style={getListStyle(snapshot.isDraggingOver)}>
                            {this.state.ostoskori.map((item, index) => (
                                <Draggable
                                    key={index}
                                    draggableId={item.tuote + index + "O"}
                                    index={index}>
                                    {(provided, snapshot) => (
                                        <div
                                            ref={provided.innerRef}
                                            {...provided.draggableProps}
                                            {...provided.dragHandleProps}
                                            style={getItemStyle(
                                                snapshot.isDragging,
                                                provided.draggableProps.style
                                            )}>
                                           {item.tuote}
                                        </div>
                                    )}
                                </Draggable>
                            ))}
                            {provided.placeholder}
                        </div>
                    )}
                </Droppable>
                </div>
                <div>
               Käytetty
                <Droppable droppableId="droppable3">
                  
                    {(provided, snapshot) => (
                        <div
                            ref={provided.innerRef}
                            style={getListStyle(snapshot.isDraggingOver)}>
                            {this.state.used.map((item, index) => (
                                <Draggable
                                    key={index}
                                    draggableId={item.tuote + index + "E"}
                                    index={index}>
                                    {(provided, snapshot) => (
                                        <div
                                            ref={provided.innerRef}
                                            {...provided.draggableProps}
                                            {...provided.dragHandleProps}
                                            style={getItemStyle(
                                                snapshot.isDragging,
                                                provided.draggableProps.style
                                            )}>
                                           {item.tuote}
                                        </div>
                                    )}
                                </Draggable>
                            ))}
                            {provided.placeholder}
                        </div>
                    )}
                </Droppable>
                </div>
                <div>
                Hävikki

                <Droppable droppableId="droppable4">
                  
                    {(provided, snapshot) => (
                        <div 
                            ref={provided.innerRef}
                            style={getListStyle(snapshot.isDraggingOver)}>
                            {this.state.waste.map((item, index) => (
                                <Draggable
                                    key={index}
                                    draggableId={item.tuote + index + "W"}
                                    index={index}>
                                    {(provided, snapshot) => (
                                        <div
                                            ref={provided.innerRef}
                                            {...provided.draggableProps}
                                            {...provided.dragHandleProps}
                                            style={getItemStyle(
                                                snapshot.isDragging,
                                                provided.draggableProps.style
                                            )}>
                                           {item.tuote}
                                        </div>
                                    )}
                                </Draggable>
                            ))}
                            {provided.placeholder}
                        </div>
                    )}
                </Droppable>
                </div>
            </DragDropContext>
            </div>
        );
    }
}

// Put the things into the DOM!
export default App;
