Original example repo https://github.com/joseglego/react-quaggajs

Bareboned react app used for testing Quagga in react and creating calendar events.

Used packages:
- Quagga
- Cheerio
- react-add-to-calendar
- react-datepicker

npm install  
npm start

**Important**

If app being accessed from other than localhost, Quagga need HTTPS connection to access users camera.
https://create-react-app.dev/docs/using-https-in-development/
